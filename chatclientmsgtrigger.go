package chatclientmsgtrigger

import (
	"context"
	"log"
	"time"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/db"
)

type Payload struct {
	Message string `json:"message"`
	SendDate time.Time `json:"sendData"`
	Type string `json:"type"`
	User string `json:"user"`
	
}
type Room struct {

	Id string `json:"id"`
	Chats []Payload `json:"chats"`

}

// FirestoreEvent is the payload of a Firestore event.
type FirestoreEvent struct {
	OldValue   FirestoreValue `json:"oldValue"`
	Value      FirestoreValue `json:"value"`
	UpdateMask struct {
		FieldPaths []string `json:"fieldPaths"`
	} `json:"updateMask"`
}

// FirestoreValue holds Firestore fields.
type FirestoreValue struct {
	CreateTime time.Time `json:"createTime"`
	// Fields is the data for this value. The type depends on the format of your
	// database. Log an interface{} value and inspect the result to see a JSON
	// representation of your database fields.
	Fields     Climessage    `json:"fields"`
	Name       string    `json:"name"`
	UpdateTime time.Time `json:"updateTime"`
}

// Review represents the Firestore schema of a movie review.
// type Review struct {
// 	Author struct {
// 		Value string `json:"stringValue"`
// 	} `json:"author"`
// 	Text struct {
// 		Value string `json:"stringValue"`
// 	} `json:"text"`
// }

type Climessage struct {

	Chatuuid struct {
		Value string `json:"stringValue"`
	} `json:"chatuuid"`
	City struct {
		Value string `json:"stringValue"`
	} `json:"city"`
	Mob struct {
		Value string `json:"stringValue"`
	} `json:"mob"`
	Name struct {
		Value string `json:"stringValue"`
	} `json:"name"`	
	Nguid struct {
		Value string `json:"stringValue"`
	} `json:"nguid"`	
	Phone struct {
		Value string `json:"stringValue"`
	} `json:"phone"`
	Text struct {
		Value string `json:"stringValue"`
	} `json:"text"`	
	
}


var client *db.Client

func init() {
	ctx := context.Background()

	conf := &firebase.Config{
		DatabaseURL: "https://fir-vuechat4.firebaseio.com/",
	}
	app, err := firebase.NewApp(ctx, conf)
	if err != nil {
		log.Fatalf("firebase.NewApp: %v", err)
	}

	client, err = app.Database(ctx)
	if err != nil {
		log.Fatalf("app.Firestore: %v", err)
	}
}

// ScoreReview generates the scores for movie reviews and transactionally writes them to the
// Firebase Realtime Database.
func ClMsg(ctx context.Context, e FirestoreEvent) error {
	review := e.Value.Fields
	log.Println(review)
	reviweScore := score(review.Chatuuid.Value)

	ref := client.NewRef("room").Child(review.Chatuuid.Value+"/chats")
	// updateTxn := func(node db.TransactionNode) (interface{}, error) {
	// 	// var currentScore string
	// 	// if err := node.Unmarshal(&currentScore); err != nil {
	// 	// 	return nil, err
	// 	// }
	// 	// return currentScore + reviweScore, nil
	// 	return reviweScore, nil
	// }
	// return ref.Transaction(ctx, updateTxn)
	// return ref.Set(ctx, reviweScore)
	ref.Push(ctx, reviweScore)
	return nil


}

// score computes the score for a review text.
//
// This is currently just the length of the text.
func score(text string) Payload {

	log.Println("retroom.Id",text)
	now := time.Now()
	// https: //ippayment.info/api
	// resp, err := http.Get("https://ippayment.info/api")
	// if err != nil {
	// 	// handle error
	// }
	// defer resp.Body.Close()

	// body, err := ioutil.ReadAll(resp.Body)
	// if err != nil {
	// 	log.Fatalln(err)
	// }

	// return string(body)
	// log.Println("LOGLOG")
	// var retroom Room
	retload := Payload{"Hei!",now,"newmsg","Tiina"}

	// retroom.Id = "text"
	// retroom.Chats = append(retroom.Chats ,retload)	
	
	return retload

}
